<h1>Documentation</h1>

<h3>Cloud Services</h3>
we used this following services as our infrastructure :<br/>
    - Cloud Run to run the back-end of the apps <br/>
    - Cloud SQL (PostgreSQL) to store user data and it's reports <br/>
    - Cloud storage to store images data

<h3>Deployment:</h3> 
We using CI/CD to automate deployment.
The following is the workflow :

1. Setup Go and download the dependencies
2. test use this following code
   
>   docker run -d -p 33768:33768 muktiarafi/rodavis-predict-api-stub
          export PREDICT_API_URL=http://localhost:33768/predict
          go test -v ./...


3. Build docker image and push the image
4. deploy to cloud run using this code

>   gcloud run deploy rodavis-api \
          --project ${{ secrets.GCP_PROJECT_ID }} \
          --image $IMAGE_NAME \
          --region asia-southeast2 \
          --platform managed \
          --allow-unauthenticated \
          --add-cloudsql-instances ${{ secrets.INSTANCE_CONNECTION_NAME }} \
          --set-env-vars="INSTANCE_CONNECTION_NAME=${{ secrets.INSTANCE_CONNECTION_NAME }}" \
          --set-env-vars="DB_HOST=${{ secrets.DB_HOST }}" \
          --set-env-vars="DB_PORT=${{ secrets.DB_PORT }}" \
          --set-env-vars="DB_USER=${{ secrets.DB_USER }}" \
          --set-env-vars="DB_PASSWORD=${{ secrets.DB_PASSWORD }}" \
          --set-env-vars="DB_NAME=${{ secrets.DB_NAME }}" \
          --set-env-vars="JWT_KEY=${{ secrets.JWT_KEY }}" \
          --set-env-vars="PREDICT_API_URL=${{ secrets.PREDICT_API_URL }}"




